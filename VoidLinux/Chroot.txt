Автор руководства Константин t.me/freeconst

Руководство установки Void Linux EFI Btrfs на SSD через chroot, с субтомами и без шифрования.

Грузишься в Live и логинишься под рутом
(удобнее сразу запустить bash для человеческого дополнения по TAB):
su -c bash

Делаем разметку диска:
fdisk /dev/nvme0n1

Создаём 2 раздела для EFI и BTRFS:
g, n, 500M, t, uefi, n, Enter-Enter

Форматируем разделы:
mkfs.vfat -n EFI -F32 /dev/nvme0n1p1
mkfs.btrfs -L ROOT /dev/nvme0n1p2

Монтируем BTRFS, создаём сабвольюмы:
BTRFS_OPTS="rw,noatime,ssd,compress=zstd,space_cache,commit=120"
mount -o $BTRFS_OPTS /dev/nvme0n1p2 /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@snapshots

Дальше работаем уже с корневым сабвольюмом:
umount /mnt
mount -o $BTRFS_OPTS,subvol=@ /dev/nvme0n1p2 /mnt

mkdir -p /mnt/home
mount -o $BTRFS_OPTS,subvol=@home /dev/nvme0n1p2 /mnt/home

mkdir -p /mnt/.snapshots
mount -o $BTRFS_OPTS,subvol=@snapshots /dev/nvme0n1p2 /mnt/.snapshots

Создаём сабвольюмы, которые не будут попадать в снапшоты:
mkdir -p /mnt/var/cache
btrfs subvolume create /mnt/var/cache/xbps
btrfs subvolume create /mnt/var/tmp
btrfs subvolume create /mnt/srv

Монтируем EFI:
mkdir -p /mnt/boot/efi/
mount /dev/nvme0n1p1 /mnt/boot/efi/

Выбираем зеркало, нужную архитектуру, записываем в переменные:
REPO=https://repo-default.voidlinux.org/current
ARCH=x86_64

Копируем ключи с установочного диска:
mkdir -p /mnt/var/db/xbps/keys
cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/

Разворачиваем базовую систему:
XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO" base-system

Монтируем необходимые директории хоста и чрутимся в систему:
mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc

cp /etc/resolv.conf /mnt/etc/

BTRFS_OPTS=$BTRFS_OPTS PS1='(chroot) # ' chroot /mnt/ /bin/bash

Редактируем следующие файлы:
/etc/hostname - вписываем имя хоста
/etc/rc.conf - локальные настройки
/etc/default/libc-locales - раскомментируем нужные локали

Генерируем локали:
xbps-reconfigure -f glibc-locales

Задаём пароль рута:
passwd

Собираем FSTAB:
EFI_UUID=$(blkid -s UUID -o value /dev/nvme0n1p1)
ROOT_UUID=$(blkid -s UUID -o value /dev/nvme0n1p2)
cat <<EOF > /etc/fstab
UUID=$ROOT_UUID / btrfs $BTRFS_OPTS,subvol=@ 0 1
UUID=$EFI_UUID /boot/efi vfat defaults,noatime 0 2
UUID=$ROOT_UUID /home btrfs $BTRFS_OPTS,subvol=@home 0 2
UUID=$ROOT_UUID /.snapshots btrfs $BTRFS_OPTS,subvol=@snapshots 0 2
tmpfs /tmp tmpfs defaults,nosuid,nodev 0 0
EOF

Создаём свап-сабвольюм и подключаем (опционально):
btrfs subvolume create /var/swap
truncate -s 0 /var/swap/swapfile
chattr +C /var/swap/swapfile
btrfs property set /var/swap/swapfile compression none
# esli oshibka, to vvesti: chattr +m /var/swap/swapfile
chmod 600 /var/swap/swapfile
dd if=/dev/zero of=/var/swap/swapfile bs=1G count=16 status=progress
mkswap /var/swap/swapfile
swapon /var/swap/swapfile

Установка GRUB:
xbps-install -S grub-x86_64-efi
grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --removable

Реконфигурим всю систему, выходим из чрута, перезагружаемся:
xbps-reconfigure -fa
exit
shutdown -r now

Базовая система установлена.
Устанавливаем шел на выбор:
xbps-install -S zsh

Создаём пользователя сразу с выбранным шелом и задаём ему пароль:
useradd -m -G wheel -s /bin/zsh <username>
passwd <username>

Разрешаем группе wheel работать с sudo (раскомментировать строку с %wheel):
visudo

Дальше логинимся под юзером и устанавливаем нужные драйверы, рабочие окружения и т.д.
