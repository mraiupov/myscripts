#!/bin/bash

echo 'Cтавим GDM'
pacman -S gdm --noconfirm
systemctl enable gdm.service

echo 'Драйвера видеодрайверов Nvidia'
pacman -S nvidia vulkan-tools vulkan-icd-loader lib32-nvidia-utils lib32-vulkan-icd-loader --noconfirm

echo 'Установка аудиодрайверов'
pacman -S alsa-lib alsa-utils pulseaudio-alsa pulseaudio-bluetooth bluez bluez-utils gnome-bluetooth lib32-alsa-plugins lib32-curl --noconfirm

systemctl enable bluetooth.service

echo 'Установить рабочий стол Gnome'
pacman -S gnome gnome-tweaks gnome-disk-utility gnome-sound-recorder --noconfirm

echo 'Установка Wine'
pacman -S wine wine-gecko wine-mono wine-nine vkd3d multilib-devel winetricks lib32-vkd3d --noconfirm

echo 'Установка Steam и Litris'
pacman -S steam lutris --noconfirm

echo 'Ставим дополнительные программы'
pacman -S ffmpeg gparted android-tools scrcpy chromium vivaldi vivaldi-ffmpeg-codecs opera opera-ffmpeg-codecs vlc x265 gimp krita kdenlive libreoffice libreoffice-fresh-ru obs-studio audacity qbittorrent uget inkscape handbrake yt-dlp --noconfirm

echo 'Чистка кэша от всех обновлений и установочных файлов'
pacman -Sc --noconfirm
pacman -Scc --noconfirm

echo 'Установка завершена! Перезагрузите систему.'
exit
