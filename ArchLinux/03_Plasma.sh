#!/bin/bash

echo 'Cтавим SDDM'
pacman -S sddm sddm-kcm --noconfirm
systemctl enable sddm

echo "[General]" > /etc/sddm.conf
echo "# Halt command" >> /etc/sddm.conf
echo "HaltCommand=/bin/systemctl poweroff" >> /etc/sddm.conf
echo "# Initial NumLock state" >> /etc/sddm.conf
echo "# Valid values: on|off|none" >> /etc/sddm.conf
echo "# If property is set to none, numlock won't be changed Numlock=on" >> /etc/sddm.conf
echo "Numlock=on" >> /etc/sddm.conf
echo "# Reboot command" >> /etc/sddm.conf
echo "RebootCommand=/bin/systemctl reboot" >> /etc/sddm.conf

echo 'Драйвера видеодрайверов Nvidea'
echo 'pacman -S nvidia vulkan-tools vulkan-icd-loader lib32-nvidia-utils lib32-vulkan-icd-loader vkd3d lib32-vkd3d cuda ffnvcodec-headers --noconfirm'

echo 'Драйвера видеодрайверов Intel'
pacman -S mesa lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader --noconfirm

echo 'Установка аудиодрайверов'
pacman -S alsa-lib alsa-utils lib32-alsa-plugins lib32-curl bluez bluedevil pulseaudio-alsa pulseaudio-bluetooth bluez-utils plasma-nm --noconfirm

echo 'Установить рабочий стол Plasma'
pacman -S plasma-desktop plasma-pa konsole powerdevil kscreen khotkeys kinfocenter plasma-thunderbolt plasma-vault plasma-workspace-wallpapers xdg-desktop-portal xdg-desktop-portal-kde --noconfirm

systemctl enable bluetooth.service

echo 'Установка Wine'
echo 'pacman -S wine wine-gecko wine-mono wine-nine vkd3d multilib-devel winetricks lib32-vkd3d --noconfirm'

echo 'Установка Steam и Litris'
echo 'pacman -S steam lutris --noconfirm'

echo 'Ставим дополнительные программы'
pacman -S ffmpeg dolphin kdegraphics-thumbnailers ffmpegthumbs ark unrar kate flameshot kcalc okular gnome-disk-utility gparted gwenview kde-gtk-config kwallet-pam android-tools scrcpy grub-customizer vivaldi vivaldi-ffmpeg-codecs vlc x265 elisa gimp krita kdenlive libreoffice libreoffice-fresh-ru obs-studio audacity qbittorrent uget inkscape handbrake yt-dlp quiterss mkvtoolnix-cli mkvtoolnix-gui ruby ruby-rdoc ruby-docs imagemagick --noconfirm

echo 'Ставим дополнительные программы'
echo 'opera opera-ffmpeg-codecs'

echo 'Чистка кэша от всех обновлений и установочных файлов'
pacman -Sc --noconfirm
pacman -Scc --noconfirm

echo 'Установка завершена! Перезагрузите систему.'
exit
